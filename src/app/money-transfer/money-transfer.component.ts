import {Component, OnInit} from '@angular/core';
import {TransactionInput} from "../transaction-input";
import {Router} from "@angular/router";
import {TransactionService} from "../transaction.service";
import {AccountInput} from "../account-input";

@Component({
  selector: 'app-money-transfer',
  templateUrl: './money-transfer.component.html',
  styleUrls: ['./money-transfer.component.css']
})
export class MoneyTransferComponent implements OnInit {

  transactionInput: TransactionInput = new TransactionInput();
  sourceInput: AccountInput = new AccountInput();
  targetInput: AccountInput = new AccountInput();

  submitted = false;

  constructor(private transactionService: TransactionService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.transactionInput.sourceAccount = this.sourceInput;
    this.transactionInput.targetAccount = this.targetInput;
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  save() {
    this.transactionService
      .moneyTransfer(this.transactionInput).subscribe(data => {
        this.transactionInput.sourceAccount = this.sourceInput;
        this.transactionInput.targetAccount = this.targetInput;
        console.log(data)
        this.gotoList();
      },
      error => console.log(error));
  }

  gotoList() {
    this.router.navigate(['/accounts']);
  }

}
