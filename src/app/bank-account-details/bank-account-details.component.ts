import {Component, OnInit} from '@angular/core';
import {Account} from '../account';
import {AccountService} from '../account.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Transaction} from "../transaction";

@Component({
  selector: 'app-bank-account-details',
  templateUrl: './bank-account-details.component.html',
  styleUrls: ['./bank-account-details.component.css']
})

export class BankAccountDetailsComponent implements OnInit {
  id: number;
  account: Account;
  transactions: Array<Transaction []>;

  constructor(private route: ActivatedRoute, private router: Router,
              private accountService: AccountService) {
  }

  ngOnInit() {
    this.account = new Account();
    this.transactions = new Array<Transaction[]>();
    this.id = this.route.snapshot.params.id;

    this.accountService.getAccount(this.id)
      .subscribe(data => {
        console.log(data);
        console.log(data.transactions);
        this.account = data;
        this.transactions = data.transactions;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['accounts']);
  }

}
