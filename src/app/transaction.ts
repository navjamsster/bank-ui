export class Transaction {
  id: number;
  sourceAccountId: number;
  sourceOwnerName: string;
  targetAccountId: number;
  targetOwnerName: string;
  amount: number;
  reference: string;
  initiationDate: string;
  completionDate: string;
}
