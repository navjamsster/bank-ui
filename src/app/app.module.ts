import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CreateBankAccountComponent} from './cerate-bank-account/create-bank-account.component';
import {BankAccountListComponent} from './bank-account-list/bank-account-list.component';
import {BankAccountDetailsComponent} from './bank-account-details/bank-account-details.component';
import {MoneyTransferComponent} from './money-transfer/money-transfer.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateBankAccountComponent,
    BankAccountListComponent,
    BankAccountDetailsComponent,
    MoneyTransferComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
