import {CreateBankAccountComponent} from './cerate-bank-account/create-bank-account.component'
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BankAccountListComponent} from "./bank-account-list/bank-account-list.component";
import {BankAccountDetailsComponent} from "./bank-account-details/bank-account-details.component";
import {MoneyTransferComponent} from "./money-transfer/money-transfer.component";

const routes: Routes = [
  {path: '', redirectTo: 'createBankAccount', pathMatch: 'full'},
  {path: 'createBankAccount', component: CreateBankAccountComponent},
  {path: 'accounts', component: BankAccountListComponent},
  {path: 'accountDetails/:id', component: BankAccountDetailsComponent},
  {path: 'transfer', component: MoneyTransferComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
