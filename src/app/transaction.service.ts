import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private baseUrl = 'http://localhost:8081/api/v1/transactions';

  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:ban-types
  moneyTransfer(transactionInput: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, transactionInput);
  }
}
