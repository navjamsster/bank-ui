import {Transaction} from './transaction';

export class Account {
  id: number;
  accountNumber: number;
  bankName: string;
  ownerName: string;
  currentBalance: number;
  transactions: Array<Transaction>;
}
