import {AccountInput} from "./account-input";

export class TransactionInput {
  sourceAccount: AccountInput;
  targetAccount: AccountInput;
  amount: number;
  reference: string;
}
